package com.ondro.java;

import com.ondro.java.Field.FieldStatus;

public class Main {

    public static void main(String[] args) {
        ChessBoard chessBoard = new ChessBoard();

        Queen[] queens = new Queen[8];
        int possibleQueenY = 0;

        //each cycle belongs for one line
        for (int x = 0; x < 8; x++) {
            //each cycle belongs for one column
            if (possibleQueenY > 7) {

            }
            for (int y = possibleQueenY; y < 8; y++) {
                if (chessBoard.chessBoardFields[x][y].status != FieldStatus.UNDER_ATTACK
                    && chessBoard.chessBoardFields[x][y].status != FieldStatus.QUEEN) {
                    chessBoard.addQueen(x, y);
                    queens[x] = new Queen(y);
                    possibleQueenY = 0;
                    chessBoard.calculateAttacks();
                    break;
                }
                if (y >= 7) {
                    //remove previous queen
                    chessBoard.removeQueen(x - 1, queens[x - 1].y);
                    possibleQueenY = queens[x - 1].y + 1;
                    queens[x - 1] = null;
                    x --;
                    if (possibleQueenY > 7) {
                        chessBoard.removeQueen(x - 1, queens[x - 1].y);
                        possibleQueenY = queens[x - 1].y + 1;
                        queens[x - 1] = null;
                        x --;
                    }
                    x --;
                    chessBoard.calculateAttacks();
                }
            }
            System.out.println(chessBoard);
        }


    }


}
