package com.ondro.java;

public class Field {

    public enum FieldStatus {FREE, QUEEN, UNDER_ATTACK};

    public FieldStatus status;

    public Field() {
        this.status = FieldStatus.FREE;
    }

    @Override
    public String toString() {
        switch(status){
            case FREE:
                return "- ";
            case QUEEN:
                return "X ";
            case UNDER_ATTACK:
                return "+ ";
            default:
                return "E ";
        }
    }
}
