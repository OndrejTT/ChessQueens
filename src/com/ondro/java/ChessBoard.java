package com.ondro.java;

import com.ondro.java.Field.FieldStatus;

public class ChessBoard {

    public Field[][] chessBoardFields;

    public ChessBoard() {
        chessBoardFields = new Field[8][8];
        instantiateFields();
    }

    private void instantiateFields() {
        for (int i = 0; i < chessBoardFields.length; i++) {
            for (int j = 0; j < chessBoardFields.length; j++) {
                chessBoardFields[i][j] = new Field();
            }
        }
    }

    public void addQueen(int x, int y) {
        if (chessBoardFields[x][y].status == FieldStatus.QUEEN) {
            throw new RuntimeException("Queen is already in field[" + x + "][" + y + "]");
        } else if (chessBoardFields[x][y].status == FieldStatus.UNDER_ATTACK) {
            throw new RuntimeException("Field[" + x + "][" + y + "] is under attack");
        }
        chessBoardFields[x][y].status = FieldStatus.QUEEN;
    }

    public void removeQueen(int x, int y) {
        chessBoardFields[x][y].status = FieldStatus.FREE;
    }

    public void calculateAttacks() {
        for (int i = 0; i < chessBoardFields.length; i++) {
            for (int j = 0; j < chessBoardFields.length; j++) {
                //if field is not queen, find out if field is free or under attack
                if (chessBoardFields[i][j].status != FieldStatus.QUEEN) {
                    if (isFieldUnderAttack(i, j)) {
                        chessBoardFields[i][j].status = FieldStatus.UNDER_ATTACK;
                    } else {
                        chessBoardFields[i][j].status = FieldStatus.FREE;
                    }
                }
            }
        }
    }

    private boolean isFieldUnderAttack(int x, int y) {

        if (isFieldUnderAttackHorizontally(x, y)) {
            return true;
        }

        if (isFieldUnderAttackVertically(x, y)) {
            return true;
        }

        if (isFieldUnderAttackDiagonnalyFromUpperLeft(x, y)) {
            return true;
        }

        if (isFieldUnderAttackDiagonnalyFromBottomLeft(x, y)) {
            return true;
        }

        return false;

    }

    private boolean isFieldUnderAttackDiagonnalyFromUpperLeft(int x, int y) {

//        find position on upper edge
        int upperX = x;
        int upperY = y;
        for (int i = 0; i < chessBoardFields.length; i++) {
            if (upperX > 0 && upperY > 0) {
                upperX--;
                upperY--;
            } else {
                break;
            }
        }

//        go from uppler left to bottom right
        for (int j = 0; j < chessBoardFields.length; j++) {
            if (chessBoardFields[upperX + j][upperY + j].status == FieldStatus.QUEEN) {
                return true;
            }
            if (upperX + j >= 7 || upperY + j >= 7) {
                break;
            }
        }

        return false;
    }

    private boolean isFieldUnderAttackDiagonnalyFromBottomLeft(int x, int y) {

//        find position on bottom edge
        int bottomX = x;
        int bottomY = y;
        for (int i = 0; i < chessBoardFields.length; i++) {
            if (bottomX < 7 && bottomY > 0) {
                bottomX++;
                bottomY--;
            } else {
                break;
            }
        }

//        go from bottom left to top right
        for (int j = 0; j < chessBoardFields.length; j++) {
            if (chessBoardFields[bottomX - j][bottomY + j].status == FieldStatus.QUEEN) {
                return true;
            }
            if (bottomX - j <= 0 || bottomY + j >= 7) {
                break;
            }
        }

        return false;
    }

    private boolean isFieldUnderAttackVertically(int x, int y) {
//        go through column and check, if there is queen
        for (int j = 0; j < chessBoardFields.length; j++) {
            if (chessBoardFields[x][j].status == FieldStatus.QUEEN) {
                return true;
            }
        }
        return false;
    }

    private boolean isFieldUnderAttackHorizontally(int x, int y) {
//        go through line and check, if there is queen
        for (int i = 0; i < chessBoardFields.length; i++) {
            if (chessBoardFields[i][y].status == FieldStatus.QUEEN) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("****************\n");

        for (int i = 0; i < chessBoardFields.length; i++) {
            for (int j = 0; j < chessBoardFields.length; j++) {
                stringBuilder.append(chessBoardFields[i][j]);
            }
            stringBuilder.append("\n");
        }

        stringBuilder.append("****************\n");
        return stringBuilder.toString();
    }
}
